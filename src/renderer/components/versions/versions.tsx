import * as React from "react";
import "../../../css/version.css";
import { ItemVersion } from "../../../common/versions";

export interface VersionProps {
    items: ItemVersion[];
}

export const Versions: React.FunctionComponent<VersionProps> = (props: VersionProps) => {
    return (
        <div className="versionList">
            <div>We are using:</div>
            {props.items.map((item, idx) => {
                const key = `${idx}-${item.item}`;
                return (
                    <div key={key}>
                        <div className="versionItem versionPair">{item.item}</div>
                        <div className="versionNum versionPair">{item.version}</div>
                    </div>
                );
            })}
        </div>
    );
};
