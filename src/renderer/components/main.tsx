import * as React from "react";
import { Versions } from ".";
import { ipcRenderer } from "electron";
import { ItemVersion } from "../../common/versions";

interface MainState {
    items: ItemVersion[] | undefined;
}

export class MainComponent extends React.Component<{}, MainState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            items: undefined
        };
        ipcRenderer.on("load-versions-response", (event, items: ItemVersion[]) => {
            this.setState({ items: items });
        });
        ipcRenderer.send("load-versions");
    }

    public render(): React.ReactNode {
        const itemList: ItemVersion[] | undefined = this.state.items;
        return (
            <div className="main">
                <h1>Hello, World!</h1>
                {itemList ? <Versions items={itemList} /> : <div>Looking for versions...</div>}
            </div>
        );
    }
}
