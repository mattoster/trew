import { IpcMain, IpcMainEvent } from "electron";

export type IpcRequest = {
    requestName: string;
    request: (event: IpcMainEvent, ...args: never[]) => void;
};

export class IpcMainRequestManager {
    private readonly ipcMain: IpcMain;

    constructor(ipcMain: IpcMain) {
        this.ipcMain = ipcMain;
    }

    public registerRequests(requests: IpcRequest[]): void {
        requests.forEach((request: IpcRequest) => {
            this.ipcMain.on(request.requestName, request.request);
        });
    }
}
