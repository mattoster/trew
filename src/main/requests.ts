import { ItemVersion } from "../common/versions";
import { IpcRequest } from "./ipc-main";

export const versionRequests: IpcRequest[] = [
    {
        requestName: "load-versions",
        request: (event): void => {
            const nodeVersion: string = process.versions.node;
            const chromeVersion: string = process.versions.chrome;
            const electronVersion: string = process.versions.electron;

            const items: ItemVersion[] = [
                {
                    item: "Node.js",
                    version: nodeVersion
                },
                {
                    item: "Chromium",
                    version: chromeVersion
                },
                {
                    item: "Electron",
                    version: electronVersion
                }
            ];
            event.sender.send("load-versions-response", items);
        }
    }
];
