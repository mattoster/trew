import { app, Menu, shell } from "electron";
// eslint-disable-next-line import/no-extraneous-dependencies
import * as EDI from "electron-devtools-installer";

export const addDevToolsExtensions = async (isDevMode: boolean): Promise<void> => {
    if (!isDevMode) {
        return;
    }
    try {
        // Once shipped, this shouldn't be reachable unless NODE_ENV is being surreptitiously set to dev
        const { default: installExtension, REACT_DEVELOPER_TOOLS } = EDI;
        const name = await installExtension(REACT_DEVELOPER_TOOLS);
        console.log(`Added Extension:  ${name}`);
    } catch (error) {
        console.error(`Couldn't add dev tool extensions: ${error}`);
    }
};

export const createMenu = (isDevMode: boolean): void => {
    const isMac = process.platform === "darwin";

    const view = [
        { role: "resetzoom" },
        { role: "zoomin" },
        { role: "zoomout" },
        { type: "separator" },
        { role: "togglefullscreen" }
    ];

    const devView = [
        { role: "reload" },
        { role: "forcereload" },
        { role: "toggledevtools" },
        { type: "separator" },
        { role: "resetzoom" },
        { role: "zoomin" },
        { role: "zoomout" },
        { type: "separator" },
        { role: "togglefullscreen" }
    ];

    const template = [
        // { role: 'appMenu' }
        ...(isMac
            ? [
                  {
                      label: app.name,
                      submenu: [
                          { role: "about" },
                          { type: "separator" },
                          { role: "services" },
                          { type: "separator" },
                          { role: "hide" },
                          { role: "hideothers" },
                          { role: "unhide" },
                          { type: "separator" },
                          { role: "quit" }
                      ]
                  }
              ]
            : []), // { role: 'fileMenu' }
        {
            label: "File",
            submenu: [isMac ? { role: "close" } : { role: "quit" }]
        }, // { role: 'editMenu' }
        {
            label: "Edit",
            submenu: [
                { role: "undo" },
                { role: "redo" },
                { type: "separator" },
                { role: "cut" },
                { role: "copy" },
                { role: "paste" },
                ...(isMac
                    ? [
                          { role: "pasteAndMatchStyle" },
                          { role: "delete" },
                          { role: "selectAll" },
                          { type: "separator" },
                          {
                              label: "Speech",
                              submenu: [{ role: "startspeaking" }, { role: "stopspeaking" }]
                          }
                      ]
                    : [{ role: "delete" }, { type: "separator" }, { role: "selectAll" }])
            ]
        }, // { role: 'viewMenu' }
        {
            label: "View",
            submenu: isDevMode ? devView : view
        }, // { role: 'windowMenu' }
        {
            label: "Window",
            submenu: [
                { role: "minimize" },
                { role: "zoom" },
                ...(isMac
                    ? [{ type: "separator" }, { role: "front" }, { type: "separator" }, { role: "window" }]
                    : [{ role: "close" }])
            ]
        },
        {
            role: "help",
            submenu: [
                {
                    label: "Learn More",
                    click: async (): Promise<void> => {
                        await shell.openExternal("https://electronjs.org");
                    }
                }
            ]
        }
    ];
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);
};
