export interface ItemVersion {
    item: string;
    version: string;
}
