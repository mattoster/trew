# TypeScript, React, Electron, and Webpack
A project using TypeScript, React, Electron, and Webpack for a desktop application.

<br/>

## TypeScript and React
Using TypeScript and React will allow for static typing throughout this project. The tsconfig is also setup to allow
other features like JSX.

### Type Definitions
In the package.json there are dependency entries for React and React type definitions
```json
{
    "@types/react": "...",
    "@types/react-dom": "...",
    "react": "...",
    "react-dom": "..."
}
```
These are used to type React for use with Typescript

<br/>

## Usage

### Setup
At your shell, in the project directory, run the following to get started.
```
npm install
npm run build
```
This installs packages for use with this project. Including Typescript, webpack, React, and others. Within the resulting
`build` directory there will be an `index.html` file that will be the applications entry point.

<br/>

## Webpack
Webpack is used to create the bundles (main and renderer) that will be used for the application. It will also provide
hot updating as well as launching the main and render processes simultaneously.

The Webpack config is split between a common, dev, and prod file. The scripts in `package.json` then specify the
appropriate config file to use with the `--config` flag.

Webpack dev server is setup so that Webpack can bundle all of the **renderer** code and serve it from its own server.
The server looks for changes in the **renderer** code and then automatically rebuilds them, writes them out to the
filesystem and refreshes the application.

The **renderer** webpack config begins by triggering the `start:main:dev` script (builds and watches for changes in the
**main** bundle) before continuing to launch the webpack dev server that will bundle the **renderer** code.


Start development with hot-swapping with one command:
```bash
npm run start:dev
```

<br/>

## Electron
To use Electron IPC, node integration must be set to true. With this enabled, loading URIs in the render process could
introduce security issues.

### Electron Builder
A simple config is included for building a bundled application with `electron-builder`.

<br/>

## Jest - TODO
This project is setup to use Jest and Enzyme for tests written in Typescript. Jest is the testing framework, and enzyme
is used for shallow/full rendering of React components. Running `npm run test:coverage` will create a coverage directory
in the project containing a coverage report.

The file `src/__tests__/enzyme-setup.js` sets up the Enzyme adapter that is needed for the test runs. Jest will set this
up before the test runs.

```bash
npm run test
```
\-\- OR \-\-
```bash
npm run test:coverage
```
The above runs the tests and produces a `coverage` directory containing coverage information.
